"""
This module coordinates all the activities to complete the programming
assignment for Homework 1. It transforms the input data to a format understood
by the algorithm, trains the ID3 decision tree using the training data, and
tests the algorithm using the testing data.
"""

import data_proc
import id3
import node

if __name__ == "__main__":
    # Process the training data and the testing data.
    train_data = data_proc.transform_data("train.csv")
    train_labels = []
    for sample in train_data.get_all():
        train_labels.append(sample[-1])
    test_data = data_proc.transform_data("test.csv")
    test_labels = []
    for sample in test_data.get_all():
        test_labels.append(sample[-1])

    # Perform the testing laid out in Problem 2 of the Programming Assignment
    # for Homework 1.
    for i in xrange(7):
        print "\nInfo Gain, Max Depth = " + str(i + 1)
        ig_classifier = id3.ID3Classifier()
        ig_classifier.train(train_data, max_depth=(i + 1))

        train_predictions = ig_classifier.batch_predict(train_data)
        train_accuracy = ig_classifier.get_accuracy(train_labels,
                                                    train_predictions)
        print "\tTraining data accuracy = ", train_accuracy

        test_predictions = ig_classifier.batch_predict(test_data)
        test_accuracy = ig_classifier.get_accuracy(test_labels,
                                                   test_predictions)
        print "\tTesting data accuracy = ", test_accuracy

        print "\nMajority Error, Max Depth = " + str(i + 1)
        me_classifier = id3.ID3Classifier()
        me_classifier.train(
            train_data,
            max_depth=(i + 1),
            splitting_heuristic=node.SplittingHeuristic.MAJORITY_ERROR)

        train_predictions = me_classifier.batch_predict(train_data)
        train_accuracy = me_classifier.get_accuracy(train_labels,
                                                    train_predictions)
        print "\tTraining data accuracy = ", train_accuracy

        test_predictions = me_classifier.batch_predict(test_data)
        test_accuracy = me_classifier.get_accuracy(test_labels,
                                                   test_predictions)
        print "\tTesting data accuracy = ", test_accuracy