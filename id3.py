"""
This module contains the high level ID3 decision tree functionality.
"""

import node
import samples
import sys
import uid


class ID3Classifier(object):
    """
    The ID3 class provides ID3 decision tree capability.
    """

    def __init__(self):
        """ Constructor """
        self.tree = None
        self.samples = None

    def train(self,
              data,
              max_depth=sys.maxint,
              splitting_heuristic=node.SplittingHeuristic.INFO_GAIN):
        """
        Grows a decision tree from the training data.

        Arguments:
            max_depth:              The maximum depth to which the tree should
                                    be allowed to grow.
            splitting_heuristic:    The metric used to determine the optimal
                                    split.
        """
        self.samples = data

        # Set the root node of the tree.
        root_params = node.NodeParams()
        root_params.node_ID = uid.UIDGenerator.get_unique_ID()
        root_params.attributes_remaining = range(len(self.samples.get(0)) - 1)
        root_params.level = 1
        root_params.max_feature_values = data.max_feature_values
        root_params.largest_label = data.max_label_value
        root_params.sample_inds = range(len(self.samples.get_all()))
        self.tree = node.Node(root_params)

        # Grow the tree.
        self.tree.split(self.samples, max_depth, splitting_heuristic)

    def predict(self, sample):
        """
        Predicts the label of the input sample by following the ID3 decision
        tree until encountering a leaf node.

        Arguments:
            sample:     The sample to classify.
        Returns:
            The predicted label for the sample.
        """
        label = 0
        if not (self.tree is None):
            curr_node = self.tree
            is_leaf = False
            while not is_leaf:
                # NOT a leaf node
                if curr_node.label is None:
                    split_attr = curr_node.splitting_attribute
                    curr_node = curr_node.children[sample[split_attr]]
                # IS a leaf node
                else:
                    is_leaf = True
                    label = curr_node.label
        
        return label

    def batch_predict(self, samples):
        """
        Predicts labels for a batch of samples.

        Arguments:
            samples:    The samples to classify.
        Returns:
            A list of the predicted labels.
        """
        predictions = []
        for sample in samples.get_all():
            predictions.append(self.predict(sample))
        
        return predictions
    
    def get_accuracy(self, actual_labels, predicted_labels):
        """
        Compares a list of actual labels with a list of predicted labels for the
        same data and reports the accuracy of the classifier on this data set.

        Arguments:
            actual_labels:  The actual labels from the data set.
            predicted_labels:   The labels that were predicted by the classifier.
        Returns:
            The accuracy of the classifier as a floating point number in the
            range [0, 1].
        """
        if not (len(actual_labels) == len(predicted_labels)):
            raise ValueError("The two sets of labels are not the same size!")
        else:
            total_labels = len(actual_labels)
            total_correct = 0
            for i in xrange(total_labels):
                if actual_labels[i] == predicted_labels[i]:
                    total_correct += 1
            
            return float(total_correct) / float(total_labels)

    def display_tree(self, root):
        """
        Displays the tree on the console.

	Arguments:
		root:	The root node of the tree to display.
        """
	if not (root is None):
		indent = ""
		for i in xrange(root.node_params.level - 1):
			indent += "\t"
		print ""
		print indent + "Node " + str(root.node_params.node_ID) + ":"
		print indent + "\tLabel: " + str(root.label)
		print indent + "\tLevel: " + str(root.node_params.level)
		print indent + "\tSplitting Attribute: " + str(root.splitting_attribute)
		print indent + "\tNum samples: " + str(len(root.node_params.sample_inds))
		children_str = ""
		print indent + "\tNum children: " + str(len(root.children))
		for i in xrange(len(root.children)):
			if i == len(root.children) - 1:
				children_str += str(root.children[i].node_params.node_ID)
			else:
				children_str += str(root.children[i].node_params.node_ID) + ", "
		print indent + "\tChildren: " + children_str
		print ""

		for child in root.children:
			self.display_tree(child)
	else:
		raise ValueError("Trying to display an uninitialized Node!")

