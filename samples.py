"""
This module contains an abstraction for a list of samples.
"""

class Samples(object):
    """
    The Samples class provides an abstraction for a list of samples.
    """
    def __init__(self, samples, max_feature_values, max_label_value):
        """ Constructor """
        self.samples = samples
        self.max_feature_values = max_feature_values
        self.max_label_value = max_label_value
    
    def get(self, index):
        """ Gets the sample at the given index. """
        if index >= len(self.samples):
            raise IndexError("Index out of range! " + str(index))
        else:
            return self.samples[index]
    
    def get_all(self):
        """ Returns the list of all samples. """
        return self.samples