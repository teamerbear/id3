Use the 'run.sh' script to run my ID3 algorithm. The code takes care of running
the algorithm for tree depths of 1-7 for each splitting heuristic (information
gain and majority error). The accuracy for each method is output to the console
for the executor to see.

The 'run.sh' script takes no parameters. Simply execute it as follows:

    sh run.sh
