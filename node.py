"""
This module contains all the utilities for decision tree nodes.
"""

import copy
import enum
import math
import sys
import uid


class SplittingHeuristic(enum.Enum):
    """ An enumeration for different methods of finding the optimal data split. """
    INFO_GAIN = 0,
    MAJORITY_ERROR = 1


class NodeParams(object):
    """
    Contains all the parameters needed to initialize a Node object.

    Instance variables:
        attributes_remaining:   A list of the remaining attributes that we have
                                not yet split on.
        level:                  The level (depth) of this node in the tree.
        max_feature_values:     A list of maximum feature values. The value at
                                index i corresponds to the maximum value that
                                feature i can take.
        largest_label:          The largest possible label value. This assumes
                                that labels go from 0 to n with no holes.
        entropy:                The entropy of the given set of samples.
        sample_inds:            The indices in the list of training data that
                                are relevant to this node.
    """

    def __init__(self):
        self.attributes_remaining = []
        self.level = -1
        self.max_feature_values = []
        self.largest_label = 0
        self.entropy = 999.0
        self.sample_inds = []
        self.node_ID = -1

    @staticmethod
    def calculate_entropy(label_list, max_label):
        """
        Calculates the entropy from a list of labels.

        Arguments:
            label_list:     A list of the labels from which we will calculate
                            entropy
            max_label:      The maximum value a label can take on.
        Returns:
            The entropy of this list of labels.
        """
        if not label_list:
            return 0.0

        else:
            # Get the count of each label in our data set.
            label_counts = [0] * (max_label + 1)
            for label in label_list:
                if label > max_label:
                    raise ValueError("A label has an erroneous value!")
                else:
                    label_counts[label] += 1

            # Make the entropy calculation.
            total_samples = len(label_list)
            entropy_sum = 0.0
            for count in label_counts:
                p_count = float(
                    float(count) /
                    float(total_samples))  # Proportion of this label

                # If the proportion of this label is zero, the log function will
                # yell at us. Take that term as zero.
                if not NodeParams.isclose(p_count, 0.0):
                    entropy_sum -= p_count * math.log(p_count, 2.0)

            return entropy_sum

    @staticmethod
    def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
        """
        Determines whether two floating point numbers are close enough to be
        considered equivalent.

        See https://stackoverflow.com/questions/5595425/what-is-the-best-way-to-compare-floats-for-almost-equality-in-python
        """
        return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


class Node(object):
    """
    Represents a node in an ID3 decision tree.

    Instance variables:
        node_params:            A pointer to a NodeParams object with all the
                                parameters required to initialize a node.
        splitting_attribute:    The attribute that we chose to split on to get
                                this node's children.
        label:                  The label that this nodes represents. The label
                                is None unless the node is a leaf node.
        children:               A dictionary containing the Node's children. The
                                keys are the different values that the splitting
                                attributes can take and the values are the nodes
                                corresponding to that value.
    """

    # pylint: disable=too-many-instance-attributes

    def __init__(self, node_params):
        # Sanitize input
        self.__validate_node_params(node_params)

        self.node_params = node_params
        self.splitting_attribute = -1
        self.label = None
        self.children = []

    def __validate_node_params(self, node_params):
        """
        Raises an exception if there are any errors with the data used to
        initialize this node.

        Arguments:
            node_params:    The node parameters to validate.
        """
        # Do some basic sanitization: check for empty lists and incorrect types.
        if not isinstance(node_params, NodeParams):
            raise TypeError("Expected an instance of the NodeParams class!")
        if node_params.level < 0:
            raise ValueError("Level must be non-negative!")
        if not node_params.max_feature_values:
            raise ValueError("Maximum feature value list is empty!")
        if node_params.largest_label < 1:
            raise ValueError(
                "Data with less than two possible labels is uninteresting!")

    def split(self,
              samples,
              max_depth=sys.maxint,
              splitting_heuristic=SplittingHeuristic.INFO_GAIN):
        """
        Recursive splits the node into child nodes based on the supplied
        heuristic for measuring node pureness.

        Arguments:
            samples:                A pointer to the training data.
            max_depth:              The maximum depth the tree will be allowed
                                    to grow to.
            splitting_heuristic:    The metric used to determine the optimal
                                    split.
        Returns:
            A pointer to the root of the tree.
        """
        # Base case: If we have no samples or we have reached our depth limit or
        # there are no attributes remaining to split on or the node is 'pure',
        # we are at a leaf node.
        stop = False
        if not self.node_params.sample_inds:
            stop = True
        if (self.node_params.level >= max_depth) and not stop:
            stop = True
        if not self.node_params.attributes_remaining and not stop:
            stop = True

        # Check to see if the node is pure (i.e. all the labels are the same).
        if not stop:
            first_sample = samples.get(self.node_params.sample_inds[0])
            first_label = first_sample[-1]
            pure = True
            for index in self.node_params.sample_inds:
                sample = samples.get(index)
                if not (sample[-1] == first_label):
                    pure = False
                    break
            if pure:
                stop = True

        # If this node is a base case, apply the majority label and return a
        # pointer to its self.
        if stop:
            # If we have no samples, the label should have been assigned by the
            # parent node. Otherwise, figure out what the label should be.
            if self.node_params.sample_inds:
                parent_labels = []
                for i in self.node_params.sample_inds:
                    sample = samples.get(i)
                    sample_label = sample[-1]
                    parent_labels.append(sample_label)

                self.label = self.__find_majority_label(
                    parent_labels, self.node_params.largest_label)
            return self

        # Otherwise, choose the best split and recursively continue the tree.
        else:
            best_attribute_index = 0
            if SplittingHeuristic.MAJORITY_ERROR == splitting_heuristic:
                best_majority_err = 999999.0
                for i in xrange(len(self.node_params.attributes_remaining)):
                    majority_err = self.__calculate_majority_error(
                        samples, self.node_params.sample_inds,
                        self.node_params.attributes_remaining[i],
                        self.node_params.max_feature_values[
                            self.node_params.attributes_remaining[i]],
                        self.node_params.largest_label)
                    if majority_err < best_majority_err:
                        best_majority_err = majority_err
                        best_attribute_index = i

            # Default to information gain.
            else:
                # Start by calculating our own entropy
                label_list = []
                for index in self.node_params.sample_inds:
                    label_list.append(samples.get(index)[-1])
                self.node_params.entropy = NodeParams.calculate_entropy(
                    label_list, self.node_params.largest_label)

                # Find the index of the best attribute to split on
                best_info_gain = 0.0
                for i in xrange(len(self.node_params.attributes_remaining)):
                    info_gain = self.__calculate_info_gain(
                        samples, self.node_params.sample_inds,
                        self.node_params.entropy,
                        self.node_params.attributes_remaining[i],
                        self.node_params.max_feature_values[
                            self.node_params.attributes_remaining[i]],
                        self.node_params.largest_label)
                    if info_gain > best_info_gain:
                        best_info_gain = info_gain
                        best_attribute_index = i

            # Split on the best attribute.
            self.splitting_attribute = self.node_params.attributes_remaining[
                best_attribute_index]
            for i in xrange(self.node_params.max_feature_values[
                    self.splitting_attribute] + 1):
                # Create a modified set of node params for the child.
                child_params = NodeParams()
                child_params.node_ID = uid.UIDGenerator.get_unique_ID()
                child_params.attributes_remaining = copy.deepcopy(
                    self.node_params.attributes_remaining)
                child_params.attributes_remaining.remove(
                    self.splitting_attribute)
                child_params.largest_label = self.node_params.largest_label
                child_params.level = self.node_params.level + 1
                child_params.max_feature_values = self.node_params.max_feature_values
                child_params.sample_inds = []
                for index in self.node_params.sample_inds:
                    if samples.get(index)[self.splitting_attribute] == i:
                        child_params.sample_inds.append(index)

                # Recursively continue building the tree. If the child node has
                # no samples, assign it's label as the parent's majority label.
                child_node = Node(child_params)
                if not child_params.sample_inds:
                    parent_labels = []
                    for index in self.node_params.sample_inds:
                        label = samples.get(index)[-1]
                        parent_labels.append(label)
                    child_node.label = self.__find_majority_label(
                        parent_labels, self.node_params.largest_label)

                self.children.append(child_node)
                child_node.split(samples, max_depth, splitting_heuristic)

    def __find_majority_label(self, label_list, largest_label):
        """
        Finds the label that describes the majority of the samples provided.

        Arguments:
            label_list:     A list of labels to search.
            largest_label:  The largest possible label.
        Returns:
            The majority label.
        """
        label_counts = [0] * (largest_label + 1)
        for label in label_list:
            label_counts[label] += 1

        max_ind = 0
        max_count = label_counts[0]
        for i in xrange(len(label_counts)):
            if label_counts[i] > max_count:
                max_ind = i
                max_count = label_counts[i]

        return max_ind

    def __calculate_info_gain(self, samples, parent_sample_inds,
                              parent_entropy, attribute_index, max_attr_value,
                              max_label_value):
        """
        Calculates the information if a parent node were to split based on the
        given attribute.

        Arguments:
            parent_sample_inds:     The indices of the training data sample list
                                    that are relevant to the parent node.
            parent_entropy:         The entropy of the parent node.
            attribute_index:        The index of the attribute that we are
                                    splitting on.
            max_attr_value:         The maximum value the splitting attribute
                                    can take on.
            max_label_value:        The maximum value a label can take on.
        Returns:
            The information gain for the split.
        """
        weighted_entropy_sum = 0.0
        total_samples = len(parent_sample_inds)
        sample_labels = [[] for _ in range(max_attr_value + 1)]
        for index in parent_sample_inds:
            # Add the label of the sample to the list that corresponds to the
            # particular attribute value.
            sample = samples.get(index)
            label_list = sample_labels[sample[attribute_index]]
            label_list.append(sample[len(sample) - 1])

        for i in xrange(len(sample_labels)):
            # This is the weight term.  The number of samples where the given
            # attribute is equal to some value v divided by the number of
            # samples in the parent set.
            weight = float(len(sample_labels[i])) / float(total_samples)
            entropy = NodeParams.calculate_entropy(sample_labels[i],
                                                   max_label_value)
            weighted_entropy_sum += weight * entropy

        return parent_entropy - weighted_entropy_sum

    def __calculate_majority_error(self, samples, parent_sample_inds,
                                   attribute_index, max_attr_value,
                                   max_label_value):
        """
        Calculates the majority error of a split on a given attribute.

        Arguments:
            samples:            A pointer to the entire set of training data.
            parent_sample_inds: The indices of the training data set that are
                                relevant to the parent node.
            attribute_index:    The index of the attribute we will split on.
            max_attr_value:     The maximum value the slitting attribute can
                                take on.
            max_label_value:    The maximum value a label can take on.
        Returns:
            The majority error of the split.
        """
        majority_err_sum = 0.0
        sample_labels = [[] for _ in range(max_attr_value + 1)]
        for index in parent_sample_inds:
            # Add the label of the sample to the list that corresponds to the
            # particular attribute value.
            sample = samples.get(index)
            label_list = sample_labels[sample[attribute_index]]
            label_list.append(sample[len(sample) - 1])

        for i in xrange(len(sample_labels)):
            # Only compute the error if there are labels to work with. Otherwise,
            # consider the error as zero.
            if sample_labels[i]:
                total_labels = len(sample_labels[i])
                label_counts = [0 for _ in range(max_label_value + 1)]
                for label in sample_labels[i]:
                    label_counts[label] += 1
                majority = max(label_counts)
                weight = float(total_labels) / float(len(parent_sample_inds))
                majority_err_sum += weight * float(
                    total_labels - majority) / float(total_labels)

        return majority_err_sum
