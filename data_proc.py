"""
This module processes a comma-separated data file into a Python-usable structure.
"""

import samples


def transform_data(data_file):
    """
    Transforms the data from a textual representation into a numerical
    representation. This is accomplished by assigning a number to each
    attribute as well as each possible value an attribute can take. For example,
    if I was dealing with data that had two columns, "color" and "shape", the
    color attribute would then be considered the 0-th attribute and shape would
    be considered the 1-st attribute. If the possible colors were red, green,
    and blue, red might be assigned the index 0, green the index 1, and blue the
    index 2.

    Arguments:
        data_file:  The file containing the textual data.
    Returns:
        A list of samples in this form:
            { attribute 0, attribute 1, . . . , attribute n, label}
    """
    # Define some dictionaries to aid in text to numerical translation.
    attr_dict = {
        'buying': 0,
        'maint': 1,
        'doors': 2,
        'persons': 3,
        'lug_boot': 4,
        'safety': 5,
        'label': 6
    }

    buying_dict = {'vhigh': 0, 'high': 1, 'med': 2, 'low': 3}
    maint_dict = {'vhigh': 0, 'high': 1, 'med': 2, 'low': 3}
    doors_dict = {'2': 0, '3': 1, '4': 2, '5more': 3}
    persons_dict = {'2': 0, '4': 1, 'more': 2}
    lug_boot_dict = {'small': 0, 'med': 1, 'big': 2}
    safety_dict = {'low': 0, 'med': 1, 'high': 2}
    label_dict = {'unacc': 0, 'acc': 1, 'good': 2, 'vgood': 3}

    index_directory = {
        0: buying_dict,
        1: maint_dict,
        2: doors_dict,
        3: persons_dict,
        4: lug_boot_dict,
        5: safety_dict,
        6: label_dict
    }

    max_buying = 3
    max_maint = 3
    max_doors = 3
    max_persons = 2
    max_lug_boot = 2
    max_safety = 2
    max_label = 3

    lines = []
    with open(data_file, 'r') as infile:
        lines = infile.readlines()

    data_samples = []
    for line in lines:
        sample = line.strip().split(',')
        for i in xrange(len(sample)):
            sample[i] = index_directory[i][sample[i]]
        data_samples.append(sample)
        if not (len(sample) == 7):
            print "Sample length not equal to 7! It is " + str(len(sample))

    max_feature_values = [
        max_buying, max_maint, max_doors, max_persons, max_lug_boot, max_safety
    ]
    output_data = samples.Samples(data_samples, max_feature_values, max_label)

    """ Uncomment this section to write output data to a file. """
    file_name = data_file.split('.')[0]
    ext = data_file.split('.')[1]
    write_file = str(file_name) + "_transformed." + str(ext)
    with open(write_file, 'w') as outfile:
        for sample in data_samples:
            str_list = [str(a) for a in sample]
            outfile.write(','.join(str_list) + '\n')

    print "Processed " + str(len(data_samples)) + " data samples"
    return output_data
