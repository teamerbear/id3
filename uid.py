"""
This module contains a utility to get a unique ID with which to identify nodes
in the decision tree.
"""

class UIDGenerator(object):
    
    current_ID = 0

    @staticmethod
    def get_unique_ID():
        ret_val = UIDGenerator.current_ID
        UIDGenerator.current_ID += 1
        return ret_val
